//
//  ViewController.swift
//  Quote
//
//  Created by Jedidiah Laudenslayer on 8/5/17.
//  Copyright © 2017 Jedidiah Laudenslayer. All rights reserved.
//

import UIKit
import SwiftyJSON
import Mortar
import SDWebImage

/// Struct to contain all UI constants
fileprivate struct UIConstants {
    /// Font size for the quote in compact mode
    static let QuoteFontSizeCompact: CGFloat = 25.0
    /// Font size for the quote in regular mode
    static let QuoteFontSizeRegular: CGFloat = 35.0
    /// Font size for the author in compact mode
    static let AuthorFontSizeCompact: CGFloat = 20.0
    /// Font size for the author in regular mode
    static let AuthorFontSizeRegular: CGFloat = 30.0
    /// Size of edge padding for the quote
    static let QuotePadding = 20.0
    /// Size of top padding for the author
    static let AuthorPadding = 10.0
    /// String for cell reuse identifier
    static let GIFCellReuseId = "cell"
    /// Columns of gifs in compact mode
    static let GIFColumnsCompact = 2
    /// Columns of gifs in regular mode
    static let GIFColumnsRegular = 4
}

class ViewController: UIViewController {

    /// Access top the author top constraint
    private var authorTopConstraint: MortarConstraint?

    /// Access quote width constraint
    private var quoteWidthConstraint: MortarConstraint?

    /// Access to the current download task for cancel upon reuse
    private var currentTask: URLSessionDataTask?

    /// Label used to display the quote
    private let quoteLabel: UILabel = {
        let label = UILabel()
        label.layer.shadowOffset = .zero
        label.layer.shadowOpacity = 1.0
        label.layer.shadowRadius = 4.0
        label.layer.shadowColor = UIColor.black.cgColor
        label.textColor = .white
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = UIFont(name: "DINCondensed-Bold", size: UIConstants.QuoteFontSizeCompact)
        return label
    }()

    /// Label used to display the quote
    private let authorLabel: UILabel = {
        let label = UILabel()
        label.layer.shadowOffset = .zero
        label.layer.shadowOpacity = 1.0
        label.layer.shadowRadius = 4.0
        label.layer.shadowColor = UIColor.black.cgColor
        label.textColor = .white
        label.font = UIFont(name: "DINCondensed-Bold", size: UIConstants.AuthorFontSizeCompact)
        return label
    }()

    /// Collection view used to display the gif search results
    public let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        let collection = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collection.backgroundColor = .clear
        collection.register(GifCollectionCell.classForCoder(), forCellWithReuseIdentifier: UIConstants.GIFCellReuseId)
        return collection
    }()

    /// Gifs loaded into the backgrounds
    public private (set) var loadedGifs = [GifObject]()

    override var prefersStatusBarHidden: Bool {
        return true
    }

    override func loadView() {
        super.loadView()

        collectionView.dataSource = self
        collectionView.delegate = self

        view |+| [collectionView, quoteLabel, authorLabel]

        collectionView.m_edges |=| view
        quoteLabel.m_centerY |=| view
        quoteLabel.m_centerX |=| view
        authorLabel.m_right |=| quoteLabel.m_right

        quoteWidthConstraint = quoteLabel.m_width |=| view.m_width - (UIConstants.QuotePadding*2.0)
        authorTopConstraint = authorLabel.m_top |=| quoteLabel.m_bottom + UIConstants.AuthorPadding
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadNewQuote()
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        let horizontalSizeClass = traitCollection.horizontalSizeClass
        
        if horizontalSizeClass != previousTraitCollection?.horizontalSizeClass {
            let quoteFontSize = horizontalSizeClass == .compact ? UIConstants.QuoteFontSizeCompact : UIConstants.QuoteFontSizeRegular
            quoteLabel.font = UIFont(name: "DINCondensed-Bold", size: quoteFontSize)
            let authorFontSize = horizontalSizeClass == .compact ? UIConstants.AuthorFontSizeCompact : UIConstants.AuthorFontSizeRegular
            authorLabel.font = UIFont(name: "DINCondensed-Bold", size: authorFontSize)
        }
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        collectionView.reloadData()
    }
    
    override var canBecomeFirstResponder: Bool {
        return true
    }

    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        loadNewQuote()
    }

    private func loadNewQuote() {
        currentTask?.cancel()
        currentTask = QuoteAPI.randomQuote(completionHandler: {[weak self] quote, error in
            guard let loadedQuote = quote,
                let quote = loadedQuote.quote else {
                    /// Something happened when trying to load a random quote
                    return
            }
            self?.quoteLabel.text = quote
            self?.authorLabel.text = "- " + (loadedQuote.author ?? "UnKnown")
            let author = loadedQuote.author ?? ""
            let searchString = "\(quote) \(author)".replacingOccurrences(of: " ", with: "+")
            self?.currentTask = GiphyAPI.searchGIFS(searchString,
                completionHandler: { gifs, error in
                    if let searchedGifs = gifs {
                        self?.loadedGifs = searchedGifs
                    } else {
                        self?.loadedGifs = [GifObject]()
                    }
                    self?.collectionView.contentOffset = .zero
                    self?.collectionView.reloadData()
            })
            
        })
    }
}

/// Extension for the delegate methods of the gif collection view
extension ViewController: UICollectionViewDelegateFlowLayout {
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let horizontalSizeClass = traitCollection.horizontalSizeClass
        let totalColumns = horizontalSizeClass == .compact ? UIConstants.GIFColumnsCompact : UIConstants.GIFColumnsRegular
        let itemSize = view.width/CGFloat(totalColumns)
        return CGSize(width: itemSize, height: itemSize)
    }
}

/// Extension for the data source methods of the gif collection view
extension ViewController: UICollectionViewDataSource {

    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return loadedGifs.count
    }

    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UIConstants.GIFCellReuseId, for: indexPath)
        if let gifCell = cell as? GifCollectionCell,
            let gifURLString = loadedGifs[indexPath.item].getImage(GiphyImage.downsized_large)?.url {
                gifCell.gifURLSting = gifURLString
        }
        return cell
    }
}

fileprivate class GifCollectionCell: UICollectionViewCell {
    /// View used to display a gif images
    public let gifView: FLAnimatedImageView = {
        let imageView = FLAnimatedImageView()
        imageView.alpha = 0.0
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()

    /// Access to the current download task for cancel upon reuse
    private var currentTask: URLSessionDataTask?

    /// The url to load into the gif view
    public var gifURLSting: String = "" {
        didSet {
            gifView.sd_setImage(with: URL(string: gifURLSting),
                placeholderImage: nil,
                options: SDWebImageOptions(),
                completed: { [weak self] image, _, _, _ in
                    UIView.animate(withDuration: 0.5, animations: { [weak self] in
                        self?.gifView.alpha = 1.0
                    })
            })
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        currentTask?.cancel()
        gifView.image = nil
        gifView.alpha = 0.0
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView |+| [gifView]
        gifView.m_edges |=| contentView
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
