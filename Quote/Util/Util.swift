//
//  Util.swift
//  Quote
//
//  Created by Jedidiah Laudenslayer on 8/6/17.
//  Copyright © 2017 Jedidiah Laudenslayer. All rights reserved.
//

import Foundation
import UIKit

/// Extension for convenience accessor for UIView
extension UIView {
    var width: CGFloat {
        get {
            return frame.size.width
        }
    }
}
