//
//  APICommon.swift
//  Quote
//
//  Created by Jedidiah Laudenslayer on 8/5/17.
//  Copyright © 2017 Jedidiah Laudenslayer. All rights reserved.
//

import Foundation

/// Public type for an api complete response
public typealias APIComplete = (URLResponse?, Any?, Error?) -> Swift.Void

/// Class used as the based for wrapping api objects
public class APIObject {
    /// The data loaded into this quote object
    public private (set) var loadedData = [AnyHashable: Any]()
    /**
     Default initializer to handle create of a quote object from JSON data
     
     - Parameter data: Data created from a JSON response
     */
    init(data: [AnyHashable: Any]) {
        loadedData = data
    }
}
