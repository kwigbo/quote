//
//  QuoteAPI.swift
//  Playground
//
//  Created by Jedidiah Laudenslayer on 8/4/17.
//  Copyright © 2017 Jedidiah Laudenslayer. All rights reserved.
//

import AFNetworking

/// Public type for returning a single quote object
public typealias RequestQuoteComplete = (QuoteObject?, Error?) -> Swift.Void

/// Struct to contain all API constants
fileprivate struct QuoteAPIConstants {
    /// Common url for all API calls
    static let APIBaseURL = "https://quotes.rest"
    /// Base API for manipulating quote collections
    static let QuoteAPIBase = "quote"
    /// API sepcific to managing custom quote collection
    static let QuoteAPIList = "quote/list"
    /// API for gettting a random quote
    static let QuoteAPIRandom = "quote/random"
}

/// Struct to manage communication with the API
public struct QuoteAPI {

    /// Public var used to set the API key
    public static var APIKey = ""

    /**
     API method to create a new quote
     
     - Parameter quote: The string value for the quote.
     - Parameter author: The string value for the author.
     - Parameter tags: Array of tags for the quote.
     - Parameter completionHandler: The closure to call when the API call is complete.
     
     - Returns: A URLSessionDataTask so the request can be canceled by the caller.
     */
    @discardableResult public static func insertQuote(_ quote: String, author: String, tags: [String], completionHandler: APIComplete?) -> URLSessionDataTask? {
        return callQuoteAPI(QuoteAPIConstants.QuoteAPIBase,
            method: "PUT",
            params: ["quote": quote, "author": author, "tags": tags.flatMap({$0}).joined(separator: ",")],
            completionHandler: completionHandler)
    }

    /**
     API method to update an existing quote
     
     - Parameter quoteId: The id of the quote to update.
     - Parameter quote: The new string value for the quote.
     - Parameter author: The new string value for the author.
     - Parameter tags: Array of tags to update the quote with.
     - Parameter completionHandler: The closure to call when the API call is complete.
     
     - Returns: A URLSessionDataTask so the request can be canceled by the caller.
     */
    @discardableResult public static func updateQuote(quoteId: String, quote: String?, author: String?, tags: [String]?, completionHandler: APIComplete?) -> URLSessionDataTask? {
        var params: [AnyHashable: Any] = ["id": quoteId]
        if let quoteString = quote,
            !quoteString.isEmpty {
            params["quote"] = quoteString
        }
        if let authorString = author,
            !authorString.isEmpty {
            params["author"] = authorString
        }
        if let tagsUpdate = tags,
            tagsUpdate.count > 0 {
            params["tags"] = tagsUpdate.flatMap({$0}).joined(separator: ",")
        }
        return callQuoteAPI(QuoteAPIConstants.QuoteAPIBase,
            method: "PATCH",
            params: params,
            completionHandler: completionHandler)
    }

    /**
     API method to delete an existing quote
     
     - Parameter quoteId: The id of the quote to update.
     - Parameter completionHandler: The closure to call when the API call is complete.
     
     - Returns: A URLSessionDataTask so the request can be canceled by the caller.
     */
    @discardableResult public static func deleteQuote(quoteId: String, completionHandler: APIComplete?) -> URLSessionDataTask? {
        return callQuoteAPI(QuoteAPIConstants.QuoteAPIBase,
            method: "DELETE",
            params: ["id": quoteId],
            completionHandler: completionHandler)
    }

    /**
     API method to list all created quotes
     
     - Parameter quoteId: The id of the quote to get
     - Parameter completionHandler: The closure to call when the API call is complete.
     
     - Returns: A URLSessionDataTask so the request can be canceled by the caller.
     */
    @discardableResult public static func getQuote(quoteId: String, completionHandler: APIComplete?) -> URLSessionDataTask? {
        return callQuoteAPI(QuoteAPIConstants.QuoteAPIBase,
                            method: "GET",
                            params: ["id": quoteId],
                            completionHandler: completionHandler)
    }

    /**
     API method to list all created quotes
     
     - Parameter start: Start location used for pagination of all quotes
     - Parameter completionHandler: The closure to call when the API call is complete.
     
     - Returns: A URLSessionDataTask so the request can be canceled by the caller.
     */
    @discardableResult public static func listQuotes(start: Int = 0, completionHandler: APIComplete?) -> URLSessionDataTask? {
        return callQuoteAPI(QuoteAPIConstants.QuoteAPIList,
            method: "GET",
            params: ["start": start],
            completionHandler: completionHandler)
    }

    /**
     API method to list all created quotes
     
     - Parameter start: Start location used for pagination of all quotes
     - Parameter completionHandler: The closure to call when the API call is complete.
     
     - Returns: A URLSessionDataTask so the request can be canceled by the caller.
     */
    @discardableResult public static func randomQuote(completionHandler: RequestQuoteComplete?) -> URLSessionDataTask? {
        let completion: APIComplete = {_, response, error in
            guard let apiResponse = response as? [AnyHashable: Any],
                let contents = apiResponse["contents"] as? [AnyHashable: Any] else {
                    completionHandler?(nil, error)
                    return
            }
            completionHandler?(QuoteObject(data: contents), error)
        }
        return callQuoteAPI(QuoteAPIConstants.QuoteAPIRandom,
                            method: "GET",
                            params: nil,
                            completionHandler: completion)
    }
}

/// Internal methods for the quote api
fileprivate extension QuoteAPI {
    /// Common session manager for API calls
    static let sessionManager = AFURLSessionManager(sessionConfiguration: URLSessionConfiguration.default)
    /// Request serializer
    static let requestSerializer = AFHTTPRequestSerializer()

    /**
     Main method to call the quote api and attach common headers
     
     - Parameter api: The name of the API to call.
     - Parameter method: The HTTP method for the request, such as 'GET', 'PUT', 'PATCH', or 'DELETE'. Must be non nil.
     - Parameter params: The query parameters to be attached to the request.
     - Parameter completionHandler: The closure to call with the API response.
     
     - Returns: A URLSessionDataTask so the request can be canceled by the caller.
     */
    static func callQuoteAPI(_ api:String, method: String, params: [AnyHashable: Any]?, completionHandler: APIComplete?) -> URLSessionDataTask? {
        let requestURLString = [QuoteAPIConstants.APIBaseURL, api].flatMap({$0}).joined(separator: "/")
        var apiRequest = requestSerializer.request(withMethod: method, urlString: requestURLString, parameters: params, error: nil)
        addCommonRequestHeaders(&apiRequest)
        let dataTask = sessionManager.dataTask(with: apiRequest as URLRequest, completionHandler: completionHandler)
        dataTask.resume()
        return dataTask
    }

    /**
     Method used to attach common HTTP header fields to a url request.
     
     - Parameter urlRequest: The name of the API to call.
     */
    static func addCommonRequestHeaders(_ urlRequest: inout NSMutableURLRequest) {
        urlRequest.setValue(APIKey, forHTTPHeaderField: "X-TheySaidSo-Api-Secret")
        urlRequest.setValue("application/json", forHTTPHeaderField: "Accept")
    }
}

/// Object used to represent a single quote from the API
public class QuoteObject: APIObject {
    /// Access to the quote string loaded into this object
    public var quote: String? {
        return loadedData["quote"] as? String
    }

    /// Access to the author string loaded into this object
    public var author: String? {
        return loadedData["author"] as? String
    }
}
