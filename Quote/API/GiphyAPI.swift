//
//  GiphyAPI.swift
//  Quote
//
//  Created by Jedidiah Laudenslayer on 8/5/17.
//  Copyright © 2017 Jedidiah Laudenslayer. All rights reserved.
//

import AFNetworking
import FLAnimatedImage

/// Public type for returning an array of GifObjects
public typealias GifSearchComplete = ([GifObject]?, Error?) -> Swift.Void

/// Public type for returning a downloaded FLAnimatedImage
public typealias GifDownloadComplete = (FLAnimatedImage?) -> Swift.Void

/// Enum to indicate Giphy rating
public enum GiphyRating: String {
    case y = "y"
    case g = "g"
    case pg = "pg"
    case pg13 = "pg-13"
    case r = "r"
    case unrated = "unrated"
    case nsfw = "nsfw"
}

/// Enum to indicate image url types
public enum GiphyImage: String {
    case downsized = "downsized"
    case downsized_large = "downsized_large"
    case downsized_medium = "downsized_medium"
    case downsized_small = "downsized_small"
    case downsized_still = "downsized_still"
    case fixed_height = "fixed_height"
    case fixed_height_downsampled = "fixed_height_downsampled"
    case fixed_height_small = "fixed_height_small"
    case fixed_height_small_still = "fixed_height_small_still"
    case fixed_height_still = "fixed_height_still"
    case fixed_width = "fixed_width"
    case fixed_width_downsampled = "fixed_width_downsampled"
    case fixed_width_small = "fixed_width_small"
    case fixed_width_small_still = "fixed_width_small_still"
    case fixed_width_still = "fixed_width_still"
    case original = "original"
    case original_still = "original_still"
    case preview_gif = "preview_gif"
}

/// Struct to contain all API constants
fileprivate struct GiphyAPIConstants {
    /// URL to be used for search
    static let APISearchURL = "https://api.giphy.com/v1/gifs/search"
}

/// Struct to manage communication with the Giphy API
public struct GiphyAPI {

    /// Public var used to set the API key
    public static var APIKey = ""

    /// Public var used to set the API language
    public static var APILang = "en"

    /// Public var used to set the API Rating
    public static var APIRating = GiphyRating.g

    /**
     Method used to download animated gif data for display with FLAnimatedfImageView
     
     - Parameter gifURLString: The url string of the gif data
     - Parameter completionHandler: The closure to call with the image data.
     
     - Returns: A URLSessionDataTask so the request can be canceled by the caller.
     */
    @discardableResult static func gifData(_ gifURLString:String, completionHandler: GifDownloadComplete?) -> URLSessionDataTask? {
        return imageSessionManager.get(gifURLString, parameters: nil, progress: nil, success: { _, response in
            guard let imageData = response as? Data,
                let animatedImage = FLAnimatedImage(animatedGIFData: imageData) else {
                    completionHandler?(nil)
                    return
            }
            completionHandler?(animatedImage)
        }, failure: { _, error in
            print(error.localizedDescription)
        })
    }

    /**
     API method to search for GIFs
     
     - Parameter search: The string value for the quote.
     - Parameter limit: The amount of results per page.
     - Parameter offset: The offset from the first result. Used for pagination.
     - Parameter completionHandler: The closure to call when the API call is complete.
     
     - Returns: A URLSessionDataTask so the request can be canceled by the caller.
     */
    @discardableResult public static func searchGIFS(_ q: String, limit: Int = 26, offset: Int = 0, completionHandler: GifSearchComplete?) -> URLSessionDataTask? {
        let completion: APIComplete = {_, response, error in
            guard let apiResponse = response as? [AnyHashable: Any],
                let data = apiResponse["data"] as? [[AnyHashable: Any]] else {
                    completionHandler?(nil, error)
                    return
            }
            completionHandler?(data.map { GifObject(data: $0) }, error)
        }
        return callGiphyAPI(GiphyAPIConstants.APISearchURL,
            params: ["q": q,
                     "limit": limit,
                     "offset": offset,
                     "rating": APIRating.rawValue,
                     "lang": APILang,
                     "api_key": APIKey],
            completionHandler: completion)
    }
}

/// Internal methods for the giphy api
fileprivate extension GiphyAPI {
    /// Common session manager for API calls
    fileprivate static let imageSessionManager: AFHTTPSessionManager = {
        let manager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        manager.responseSerializer = AFHTTPResponseSerializer()
        return manager
    }()
    /// Common session manager for API calls
    fileprivate static let sessionManager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)

    /// Request serializer
    fileprivate static let requestSerializer = AFHTTPRequestSerializer()

    /**
     Main method to call the quote api and attach common headers
     
     - Parameter api: The name of the API to call.
     - Parameter method: The HTTP method for the request, such as 'GET', 'PUT', 'PATCH', or 'DELETE'. Must be non nil.
     - Parameter params: The query parameters to be attached to the request.
     - Parameter completionHandler: The closure to call with the API response.
     
     - Returns: A URLSessionDataTask so the request can be canceled by the caller.
     */
    static func callGiphyAPI(_ apiURLString:String, params: [AnyHashable: Any]?, completionHandler: APIComplete?) -> URLSessionDataTask? {
        let apiRequest = requestSerializer.request(withMethod: "GET", urlString: apiURLString, parameters: params, error: nil)
        let dataTask = sessionManager.dataTask(with: apiRequest as URLRequest, completionHandler: completionHandler)
        dataTask.resume()
        return dataTask
    }
}

/// Object used to represent a single gif from the API
public class GifObject: APIObject {
    public func getImage(_ type: GiphyImage) -> GifDetails? {
        guard let images = loadedData["images"] as? [AnyHashable: Any],
            let image = images[type.rawValue] as? [AnyHashable: Any] else {
                return nil
        }
        return GifDetails(data: image)
    }
}

/// Object used to represent a single gif from the API
public class GifDetails: APIObject {
    public var url: String {
        return loadedData["url"] as? String ?? ""
    }
}
